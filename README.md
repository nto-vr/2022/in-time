HTC VIVE
Реализовано 3 модуль:
вторая пушка, которая стреляет патронами после зараядки/перезарядки, для выстрела пушка должна быть заряжена
выстрел производится при нажатии лучом указателя на пушку (кнопки с боков контроллера)
отключение света с шансом 1/4, раз в 20 секунд, для починки нужно воспользоваться мультитулом поднеся его к подсвеченой зоне в коридоре станции
меню с возможностью взять в руку фонарик и мультитул, фонарик выполняет свою функцию, также их можно положить обратно в меню
меню привязано к руке, появляется/изчезает при нажатии на кнопку "меню" на контроллере
рандомизация появлющихся астероидов, а именно: модель(1 из 3), поворот, размер, техтура, цвет (в ограниченых допустимых значениях)

Реализовано 4 модуль:
Каждое попадание астероида в корабль должно вызывать поломки на корабле и неполадки. (гаснет свет)
Попадание астероида должно сопровождаться звуковым сигналом.
Каждое новое попадание астероида должно сказываться на функциональном и визуальном состоянии корабля.
Текущее состояние корабля должно быть отражено на дисплее. (дисплей привязан к руке)
Космический корабль должен терпеть крушение после попадания в него третьего астероида.
Должно быть реализовано визуальное отображение количества отраженных астероидов при крушении корабля для пользователя.
