using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public GameObject[] objects;
    public Texture2D[] textures;
    public static float spawnRate = 5f;
    float nextSpawn = 20.0f;

    void Update()
    {
        if (Time.time > nextSpawn)
        {
            nextSpawn = Time.time + spawnRate;
            Vector3 whereToSpawn = new Vector3(Random.Range(-150.0f, -50.0f), Random.Range(-1.0f, 10.0f), Random.Range(-20.0f, 25.0f));
            int sc = Random.Range(50, 500);

            Texture2D tex = textures[Random.Range(0, 3)];
            List<Color> colors = new List<Color> { Color.red, Color.blue, Color.green };
            Color color = colors[Random.Range(0, 3)];

            GameObject newAsteroid = Instantiate(objects[Random.Range(0, 3)], whereToSpawn, Quaternion.identity);
            newAsteroid.transform.localScale = new Vector3(sc, sc, sc);
            newAsteroid.GetComponent<Renderer>().material.color = color;
            newAsteroid.GetComponent<Renderer>().material.mainTexture = tex;
        }
        
    }

}