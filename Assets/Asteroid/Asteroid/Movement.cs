using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float speed;
    void Update()
    {
        transform.Translate(Time.deltaTime * new Vector3(10, 0, 0) * speed);
    }

}
