using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstDestroy : MonoBehaviour
{
    public GameObject explotion;
    public AudioSource audio;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Asteroid")
        {
            Instantiate(explotion, other.transform.position, other.transform.rotation);
            audio.Play();
            GameController.astCrashed += 1;
            Destroy(other.gameObject);
        }
    }
}
