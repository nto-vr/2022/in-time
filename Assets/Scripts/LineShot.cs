using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;
public class LineShot : MonoBehaviour
{
    public InputActionReference toogleref = null;
    public GameObject menu;
    public XRSocketInteractor socket1;
    public XRSocketInteractor socket2;

    private void Awake()
	{
        toogleref.action.started += Toogle;
	}
	private void OnDestroy()
	{
        toogleref.action.started -= Toogle;
    }
	void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void Toogle(InputAction.CallbackContext context)
	{
        bool IsActive = !menu.activeSelf;
        //socket1.GetOldestInteractableSelected().transform.gameObject.SetActive(IsActive);
        //socket2.GetOldestInteractableSelected().transform.gameObject.SetActive(IsActive);
        menu.SetActive(IsActive);
    }
}
