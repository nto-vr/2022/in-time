using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] private float time;
    public Vector3 attach;
    private Vector3 attachPlayer;
    private GameObject mainCamera;
    public Transform pointer;
    public Material red;
    public Material green;
    private bool PerActive;
    public XRSocketInteractor socket;
    public GameObject Gunatach;
    public GameObject shot;
    private float _timeLeft = 60f;
    private bool _timerOn = true;
    public GameObject light;
    private bool gg = false;
    public static int astCrashed = 0;
    public int astDestroy;
    public Text ast;
    public Image[] shiphealth;
    public GameObject explotion;
    private GameObject player;
    public GameObject exitObj;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        _timeLeft = time;
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
    }

    // Update is called once per frame

    public void Periscope(bool isActive, Vector3 attach)
    {
        player.transform.position = attach;
        player.GetComponentInChildren<ContinuousMoveProviderBase>().enabled = isActive;
        GameObject.Find("LeftHand Controller").GetComponent<XRRayInteractor>().enabled = isActive;
        GameObject.Find("RightHand Controller").transform.GetChild(0).gameObject.SetActive(isActive);
        player.transform.GetChild(0).gameObject.GetComponent<TeleportationProvider>().enabled = isActive;
        player.transform.GetChild(0).gameObject.GetComponent<ActionBasedContinuousMoveProvider>().enabled = isActive;
        PerActive = !isActive;
        Debug.Log(PerActive);
    }
    public void Update()
    {

        Ray ray = new Ray(mainCamera.transform.position, mainCamera.transform.forward);
        Debug.DrawRay(mainCamera.transform.position, mainCamera.transform.forward * 100f, Color.yellow);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit) && PerActive == true)
        {
            pointer.gameObject.SetActive(true);
            if (hit.transform.tag == "Asteroid")
            {
                pointer.GetComponent<MeshRenderer>().material = green;
                StartCoroutine(destroydelay(hit.collider.gameObject));
            }
            else
            {
                pointer.GetComponent<MeshRenderer>().material = red;
            }

            pointer.position = hit.point;

        }
        else
        {
            pointer.gameObject.SetActive(false);
        }

        if (_timerOn)
        {
            _timeLeft -= Time.deltaTime;
            if (_timeLeft <= 0)
            {
                int f = Random.Range(1, 10);
                if (f == 2)
                {
                    light.GetComponent<Light>().intensity = 0;
                    gg = true;
                }
                _timeLeft = time;
            }
        }
        if (astCrashed == 1)
        {
            shiphealth[0].GetComponent<Image>().color = Color.red;
            light.GetComponent<Light>().intensity = 0;
            gg = true;
        }
        else if (astCrashed == 2)
        {
            shiphealth[1].GetComponent<Image>().color = Color.red;
            light.GetComponent<Light>().intensity = 0;
        }
        else if (astCrashed == 3)
        {

            mainCamera.transform.GetChild(0).gameObject.SetActive(true);
            shiphealth[2].GetComponent<Image>().color = Color.red;
        }
        ast.text = astDestroy.ToString();
    }
    public void Shot()
    {
        GameObject patron = socket.GetOldestInteractableSelected().transform.gameObject;
        Destroy(patron);
        Instantiate(shot, Gunatach.transform.position, Gunatach.transform.rotation);
        Debug.Log("Shot");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (gg == true && other.tag == "Tool")
        {
            _timerOn = false;
            light.GetComponent<Light>().intensity = 1;
            gg = false;
        }
    }
    public void Restart()
	{
        exitObj.SetActive(false);
        SceneManager.LoadScene("Main");
	}
    public void PerEnter()
	{
        attachPlayer = player.transform.position;
        Periscope(false, attach);
        exitObj.SetActive(true);
	}
    public void PerExit()
    {
        exitObj.SetActive(false);
        Periscope(true, attachPlayer);
    }
    public IEnumerator destroydelay(GameObject hit)
    {
        yield return new WaitForSeconds(1);
        Instantiate(explotion, hit.transform.position, hit.transform.rotation);
        Destroy(hit);
        astDestroy += 1;
    }
}