using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeValue : MonoBehaviour
{
    private AudioSource audioosrc;
    private float musicVolume = 1f;
    // Start is called before the first frame update
    void Start()
    {
        audioosrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        audioosrc.volume = musicVolume;
    }

    public void SetVolume(float vol)
    {
        musicVolume = vol;
    }
	public void Quit()
	{
        Application.Quit();
	}
}
